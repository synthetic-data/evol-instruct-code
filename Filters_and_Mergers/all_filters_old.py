# ====== v3 ======

import json
import re
from TextGenerationInference import TGI, GenerateRequest, GenerateParameters


# Load instructions from a JSON file
def load_instructions(file_path):
    with open(file_path, "r") as json_file:
        return json.load(json_file)

# Check if the rewritten_instruction is the exact same as original_instruction
def compare_instructions(original_instruction, rewritten_instruction):
    return original_instruction.strip() == rewritten_instruction.strip()

def contains_language(text, language):
    if language == "C++":
        return "C++" in text
    else:
        # Create a regex pattern to match the language as a whole word, case-insensitive
        pattern = re.compile(r'\b' + re.escape(language) + r'\b', re.IGNORECASE)
        return bool(pattern.search(text))

    
# Filter tasks based on the length of the rewritten_instruction
def length_filter(tasks):
    filtered_tasks = []
    filtered_out_tasks = []  # To track tasks filtered out by length
    for task in tasks:
        rewritten_instruction = task["rewritten_instruction"].strip()
        if rewritten_instruction and 5 <= len(rewritten_instruction) <= 700:
            filtered_tasks.append(task)
        else:
            filtered_out_tasks.append(task)
    return filtered_tasks, filtered_out_tasks

# Define the list of programming languages, including "C++" for special handling
programming_languages = ["C", "Java", "Python", "JavaScript", "C#", "PHP", "TypeScript", "Swift", "Ruby", "Objective-C", "SQL", "R", "Kotlin", "Go", "Perl", "Scala", "Haskell", "MATLAB", "Shell", "HTML", "CSS"]
# Create a regex pattern for the programming languages, excluding "C++" for now
pattern = re.compile(r'\b(' + '|'.join(map(re.escape, programming_languages)) + r')\b', re.IGNORECASE)

def looks_like_regex_or_simple_code(text):
    # Pattern that matches common regular expression indicators
    regex_pattern = re.compile(r'^\s*[\^\\\/\[\]{}()|*+?.]\s*')
    # Pattern that matches simple variable assignments or operations
    simple_code_pattern = re.compile(r'^\s*\w+\s*=\s*\w+')
    # Check for regular expression patterns or simple code
    if regex_pattern.search(text) or simple_code_pattern.search(text):
        return True
    return False


# Enhanced function to determine if a string is likely code only or not
def is_code_only(text):
    # Expanded patterns to identify code constructs and HTML
    code_patterns = [
        re.compile(r'^\s*[\w\[\]\'"].*\.encode\(.*\)$'),  # Encoding
        re.compile(r'^\s*\w+\s*=\s*.*'),  # Variable assignments
        re.compile(r'^\s*\[\w+.*\s*for\s+\w+\s+in\s+\w+\].*'),  # List comprehensions
        re.compile(r'^<\w+>.*<\/\w+>$'),  # Simple HTML tags
    ]
    # Check for lines that are exclusively code
    lines = text.strip().split('\n')
    code_lines = [line for line in lines if any(pattern.match(line) for pattern in code_patterns)]
    
    # Consider as code-only if all lines match code patterns
    return len(code_lines) == len(lines)

def clean_and_validate_output(task):
    # Enhanced cleaning: Removing trailing identifiers more reliably
    cleaned_output = re.sub(r'\s*#\b(Output|Code|Result)\b#\s*$', '', task.get("rewritten_output", "").strip(), flags=re.IGNORECASE)
    
    # Further remove any instances where these are the only content, not just trailing.
    cleaned_output = re.sub(r'^\s*#\b(Output|Code|Result)\b#\s*$', '', cleaned_output, flags=re.IGNORECASE)
    
    # Validate length after cleaning, considering non-whitespace content
    if len(cleaned_output) < 10 or not cleaned_output.strip():  # Check for non-whitespace content
        return False, ""
    return True, cleaned_output


# Main post-process function that applies all filters
def post_process_and_track(original_file_path):
    tasks = load_instructions(original_file_path)
    filtered_out_aggregate = []  # To aggregate all filtered out tasks

    # Apply initial filters: Removing exact duplicates, Length filter, and Language specific filter
    tasks_non_duplicates = [task for task in tasks if not compare_instructions(task["original_instruction"], task["rewritten_instruction"])]
    filtered_out_aggregate.extend(task for task in tasks if compare_instructions(task["original_instruction"], task["rewritten_instruction"]))

    tasks_length_filtered, tasks_filtered_out_by_length = length_filter(tasks_non_duplicates)
    filtered_out_aggregate.extend(tasks_filtered_out_by_length)
    
    tasks_language_filtered = []
    tasks_filtered_out_by_language = []
    for task in tasks_length_filtered:
        original = task['original_instruction']
        rewritten = task['rewritten_instruction']
        if "C++" in original and "C++" not in rewritten or not contains_language(rewritten, match.group() if (match := pattern.search(original)) else ""):
            tasks_filtered_out_by_language.append(task)
            continue
        tasks_language_filtered.append(task)
    filtered_out_aggregate.extend(tasks_filtered_out_by_language)
    
    # Filter based on code-only check and then clean and validate rewritten_output
    tasks_final_filtered = []
    for task in tasks_language_filtered:
        if is_code_only(task["rewritten_instruction"]):
            filtered_out_aggregate.append(task)
            continue  # Skip adding to final if it's code-only
        
        # Clean and validate rewritten_output here
        is_valid, cleaned_output = clean_and_validate_output(task)
        if is_valid:
            # Rename fields and add to final list
            new_task = {
                "instruction": task["rewritten_instruction"],
                "output": cleaned_output
            }
            tasks_final_filtered.append(new_task)  # Add to final only if valid after cleaning
        else:
            filtered_out_aggregate.append(task)

    return tasks_final_filtered, filtered_out_aggregate


def remaster_filtered_tasks(filtered_tasks, output_path):
    remastered_tasks = [{"instruction": task["rewritten_instruction"], "output": task.get("rewritten_output", "")} for task in filtered_tasks]
    with open(output_path, 'w') as json_file:
        json.dump(remastered_tasks, json_file, indent=4)

# Utility functions from the previous scripts remain unchanged
def save_filtered_tasks(filtered_tasks, output_path):
    with open(output_path, 'w') as json_file:
        json.dump(filtered_tasks, json_file, indent=4)
# Running the combined post-process function
filtered_tasks, filtered_out_tasks = post_process_and_track("/home/ec2-user/EvolInstruct/CodeLlamaInstruct70B/evolved_codealpaca_v1codellamainst70B_10gens.json")
#filtered_tasks, filtered_out_tasks = post_process_and_track("/Users/apagnoux/Documents/Code-LLM/Evoli-Alice/filtering/evolved_codealpaca_clean.json")

# Saving the filtered tasks and filtered out tasks to separate JSON files
save_filtered_tasks(filtered_tasks, 'FILTEREDevolved_codealpaca_v1codellamainst70B_10gens.json')
save_filtered_tasks(filtered_out_tasks, 'filtered_out_data_v1codellamainst70B_10gens.json')
print('Size of the good data:', len(filtered_tasks))
print('Size of the filtered out data:', len(filtered_out_tasks))






""" User
Can you create a new good_data_remastered.json that only keeps the rewritten_instruction and rewritten_output ?
Also, rename them to 'instruction' and 'output' """