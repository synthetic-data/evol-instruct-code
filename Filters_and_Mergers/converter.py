import os
import json
import glob
import logging
import dataclasses
import random
import time
from typing import Optional, Sequence


import json
import logging

def convert_alpaca_to_evol(file_path: str, lines: bool = False, output_file: str = "converted_CoMIT.json"):
    """Convert the Instruction/Input/Output format of Alpaca Instruct datasets
    to the Evol-Instruct format of Instruction/Output. Inputs are appended to the
    instructions.
    
    Args:
        file_path: the file path to a single JSON file in alpaca format
        lines: Set to True if the input is a JSONL file, the default is False
        
    Returns: a list of the instruction-output pairs generated from the alpaca set"""
    result = []
    if lines:
        with open(file_path, "r") as json_file:
            for line_number, line in enumerate(json_file, 1):
                try:
                    record = json.loads(line)
                    if record["instances"][0]["input"]:
                        record["instruction"] += '\n' + record["instances"][0]["input"]
                    result.append({
                        "instruction": record["instruction"],
                        "output": record["instances"][0]["output"]
                    })
                except json.JSONDecodeError as e:
                    logging.error(f"Error decoding JSON on line {line_number}: {str(e)}")
    else:
        try:
            with open(file_path, "r") as json_file:
                loaded_json = json.load(json_file)
            for record in loaded_json:
                if record["input"]:
                    record["instruction"] += '\n' + record["input"]
                result.append({
                    "instruction": record["instruction"],
                    "output": record["output"]
                })
        except json.JSONDecodeError as e:
            logging.error(f"Error decoding JSON: {str(e)}")
    
    with open(output_file, "w") as fp:
        json.dump(result, fp, ensure_ascii=False)
    return result


convert_alpaca_to_evol(file_path="./CoMIT.json")