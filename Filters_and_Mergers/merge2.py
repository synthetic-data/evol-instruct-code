import json

""" def load_instructions(file_path: str):
    ""Load JSON file.""
    try:
        with open(file_path, "r") as json_file:
            return json.load(json_file)
    except Exception as e:
        print(f"Error loading file {file_path}: {e}")
        return []

def load_and_merge_files(file_path1, file_path2):
    print('HELLOOO')
    # Load the first file
    data1 = load_instructions(file_path1)
    print(len(data1))
    # Load the second file
    data2 = load_instructions(file_path2)
    print(len(data2))
    # Combine the data from both files
    combined_data = data1 + data2
    
    return combined_data

# Paths to your JSON files
file_path1 = 'FILTEREDevolved_codealpaca_v1codellamainst70B.jsonl'
file_path2 = 'converted_alpaca_20k.jsonl'

# Load and merge the data from both files
merged_data = load_and_merge_files(file_path1, file_path2)


# Save the merged (and possibly filtered) data to a new JSON file
output_file = 'merged_evolved_codealpaca_v1codellamainst70B.jsonl'
with open(output_file, 'w') as file:
    json.dump(merged_data, file, indent=4)

print(f"File '{output_file}' has been created with {len(merged_data)} instructions.")
 """

import json

def merge_jsonl_files(file_path1, file_path2, output_file_path):
    with open(file_path1, 'r') as file1, open(file_path2, 'r') as file2, open(output_file_path, 'w') as output_file:
        for line in file1:
            output_file.write(line)
        for line in file2:
            output_file.write(line)

# Replace 'file1.jsonl', 'file2.jsonl', and 'merged_file.jsonl' with your actual file paths
merge_jsonl_files('FILTEREDevolved_codealpaca_v1codellamainst70B.jsonl', 'converted_alpaca_20k.jsonl', 'merged_evolved_codealpaca_v1codellamainst70B.jsonl')
