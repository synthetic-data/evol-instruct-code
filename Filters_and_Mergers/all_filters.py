# ====== v6 ======

import json
import re

# Load instructions from a JSON file
def load_instructions(file_path):
    with open(file_path, "r") as json_file:
        return json.load(json_file)

# Check if the rewritten_instruction is the exact same as original_instruction
def compare_instructions(original_instruction, rewritten_instruction):
    return original_instruction.strip() == rewritten_instruction.strip()

def contains_language(text, languages):
    for language in languages:
        if language == "C++" and "C++" in text:
            return True
        else:
            pattern = re.compile(r'\b' + re.escape(language) + r'\b', re.IGNORECASE)
            if pattern.search(text):
                return True
    return False

# Enhanced is_code_only to more accurately detect code snippets
def is_code_only(text):
    code_signatures = [
        r'\bdef\b',  # Python function
        r'\bfunction\b',  # JavaScript/PHP function
        r'^\s*<\?php',  # PHP tag
        r'<[^>]+>',  # HTML tag
        r'^\s*\w+\s*=\s*.*;',  # Variable assignment with semicolon
        r'^\s*if\s*\(',  # If statement
        r'\bclass\b',  # Class definition
        r'\bimport\b',  # Import statement
        r'^\s*\/\/',  # Single line comment
        r'^\s*\/\*',  # Block comment start
        r'^\s*SELECT\b',  # SQL query
        r'^\s*INSERT INTO\b',  # SQL insertion
        r'^\s*UPDATE\b',  # SQL update
        r'^\s*DELETE FROM\b',  # SQL delete
        r'^\s*#!/bin/bash',  # Bash script shebang
        r'^\s*#!/usr/bin/env bash',  # Env bash shebang
        r'\{\s*$',  # Opening of CSS/JS block, potentially needs refinement
        r'^\s*\w+:\s*\w+\s*;',  # CSS property:value;
    ]
    # Match any code signature
    return any(re.search(signature, text, re.IGNORECASE) for signature in code_signatures)


def clean_and_validate_output(task):
    cleaned_output = re.sub(r'\s*#\b(Output|Code|Result)\b#\s*$', '', task.get("rewritten_output", "").strip(), flags=re.IGNORECASE)
    cleaned_output = re.sub(r'^\s*#\b(Output|Code|Result)\b#\s*$', '', cleaned_output, flags=re.IGNORECASE)
    if len(cleaned_output) < 10 or not cleaned_output.strip():
        return False, ""
    return True, cleaned_output

def post_process_and_track(original_file_path):
    tasks = load_instructions(original_file_path)
    filtered_out_aggregate = []

    tasks_non_duplicates = [task for task in tasks if not compare_instructions(task["original_instruction"], task["rewritten_instruction"])]
    filtered_out_aggregate.extend(task for task in tasks if compare_instructions(task["original_instruction"], task["rewritten_instruction"]))

    # Programming languages of interest
    programming_languages = ["C", "Java", "Python", "JavaScript", "C#", "PHP", "TypeScript", "Swift", "Ruby", "Objective-C", "SQL", "R", "Kotlin", "Go", "Perl", "Scala", "Haskell", "MATLAB", "Shell", "HTML", "CSS", "C++"]

    tasks_final_filtered = []
    for task in tasks_non_duplicates:
        original = task['original_instruction']
        rewritten = task['rewritten_instruction']
        # Check language presence and if it's code-only
        if not contains_language(rewritten, programming_languages) or is_code_only(rewritten):
            filtered_out_aggregate.append(task)
            continue
        # Clean and validate rewritten_output
        is_valid, cleaned_output = clean_and_validate_output(task)
        if is_valid:
            new_task = {
                "instruction": rewritten,
                "output": cleaned_output
            }
            tasks_final_filtered.append(new_task)
        else:
            filtered_out_aggregate.append(task)

    return tasks_final_filtered, filtered_out_aggregate

def save_filtered_tasks(filtered_tasks, output_path):
    with open(output_path, 'w') as json_file:
        json.dump(filtered_tasks, json_file, indent=4)
# Running the combined post-process function
filtered_tasks, filtered_out_tasks = post_process_and_track("/home/ec2-user/EvolInstruct/all_evolution_tasks_from_alpaca_evols_v7.json")
#filtered_tasks, filtered_out_tasks = post_process_and_track("/Users/apagnoux/Documents/Code-LLM/Evoli-Alice/filtering/evolved_codealpaca_clean.json")

# Saving the filtered tasks and filtered out tasks to separate JSON files
save_filtered_tasks(filtered_tasks, '3evols_filtered_v7_aggro.json')
save_filtered_tasks(filtered_out_tasks, 'filtered_out_data_v7_aggro.json')
print('Size of the good data:', len(filtered_tasks))
print('Size of the filtered out data:', len(filtered_out_tasks))






""" User
Can you create a new good_data_remastered.json that only keeps the rewritten_instruction and rewritten_output ?
Also, rename them to 'instruction' and 'output' """