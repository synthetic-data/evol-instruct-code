import json
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import os 
import re 

def load_instructions(file_path: str):
    """Load JSON file."""
    try:
        with open(file_path, "r") as json_file:
            return json.load(json_file)
    except Exception as e:
        print(f"Error loading file {file_path}: {e}")
        return []

def calculate_ngram_similarity(instruction1, instruction2, n=8):
    """Calculate n-gram similarity between two instructions."""
    if len(instruction1.split()) < n or len(instruction2.split()) < n:
        return 0
    try:
        vectorizer = CountVectorizer(analyzer='word', ngram_range=(n, n))
        ngrams = vectorizer.fit_transform([instruction1, instruction2])
        similarity_matrix = cosine_similarity(ngrams[0], ngrams[1])
        return similarity_matrix[0][0]
    except ValueError:
        return 0

def significant_changes(original, rewritten):
    """Determine if there are significant changes between original and rewritten instructions."""
    original_lines = set(original.strip().split('\n'))
    rewritten_lines = set(rewritten.strip().split('\n'))
    added_lines = rewritten_lines - original_lines
    return len(added_lines) > 0 and not all(line.isspace() or not line for line in added_lines)

def remove_stop_words(input_string, stop_words):
    # Create a regex pattern to match stop words with optional whitespaces and line breaks around them
    stop_words_pattern = '|'.join([f"(?:\\s*{re.escape(word)}\\s*)" for word in stop_words])
    stop_regex = re.compile(stop_words_pattern, re.DOTALL | re.IGNORECASE)

    # Use the regex to find and remove the stop words and everything that follows
    result_string = stop_regex.split(input_string)[0]
    
    # Remove trailing whitespaces and line breaks
    result_string = result_string.rstrip(" \n\r\t")

    return result_string    

def filter_data(instructions):
    """Filter instructions based on similarity, significant changes, and removing stop words."""
    tasks_evolution = []
    stop_words = ["#Code#", "#Result#", "#Output#", "#Explanation#", "#Expected output:", "#Output:", "#Answer:"]  # Define your stop words
    for task in instructions:
        if not calculate_ngram_similarity(task['original_instruction'], task['rewritten_instruction'], n=8) >= 0.5 or significant_changes(task['original_instruction'], task['rewritten_instruction']):
            cleaned_output = remove_stop_words(task["rewritten_output"], stop_words)
            tasks_evolution.append({
                "instruction": task["rewritten_instruction"],
                "output": cleaned_output
            })
    return tasks_evolution



data = load_instructions("/home/ec2-user/EvolInstruct/CodeLlamaInstruct70B/evolved_codealpaca_v1codellamainst70B_nospecialprompt.json")
print('The original dataset has ', len(data), ' data')
test = filter_data(data)
#print(test)
filtered_data = 'clean_evolved_codealpaca_v1codellamainst70B_nospecialprompt.json'
loaded = load_instructions(filtered_data)
with open(filtered_data, 'w') as file:
    json.dump(test, file, indent=4)

data = load_instructions('clean_evolved_codealpaca_v1codellamainst70B.json')
print(f"'File has been created with {len(data)} instructions.")
