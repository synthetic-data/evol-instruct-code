import argparse
#import datetime
import functools
import gzip
import itertools
import json
import os
import uuid
from datetime import datetime
import subprocess
import tempfile
import time
from multiprocessing import Process
from time import sleep
from typing import Iterable, Dict

import boto3
import pandas
from marshmallow import Schema, fields
from tqdm import tqdm
import re



from TextGenerationInference import TGI, GenerateRequest, GenerateParameters


IS_SAGEMAKER_AVAILABLE = True
try:
    import sagemaker
except ModuleNotFoundError as e:
    IS_SAGEMAKER_AVAILABLE = False

IS_BOTO3_AVAILABLE = True
try:
    import boto3#
except ModuleNotFoundError as e:
    IS_BOTO3_AVAILABLE = False

import boto3
boto3.setup_default_session(profile_name="llm", region_name="us-east-1")

s3_client = boto3.client('s3')
tokenizer_path = {"gpt2": "../assets/tokenizers/gpt2.json",
                  "dd_alpha": "../assets/tokenizers/tokenizer.json"}
bucket = "tii-llm-code"
prefix = "generations"


class SampleSchema(Schema):
    task_id = fields.String(required=True)
    completion = fields.String(required=True)
    prompt = fields.String(required=False)
    
    
def trim_stop_regex(s: str, regex: re.Pattern) -> tuple[str, bool]:
    m = re.search(regex, s)
    if m is not None:
        return s[: m.span()[0]], True
    return s, False


def read_generation_code(solution_folder, benchmark_name):
    samples = []
    schema = SampleSchema()
    problems = read_problems(benchmark_name)

    for root, dirs, files in os.walk(solution_folder):
        for filename in files:
            if filename.endswith(".jsonl"):
                filename = os.path.join(root, filename)
                with open(filename) as f:
                    for line in f:
                        json_line = json.loads(line)
                        json_line['task_id'] = str(json_line['task_id'])
                        line_dict = schema.load(json_line)
                        line_dict['prompt'] = problems[line_dict["task_id"]]['prompt']
                        samples.append(line_dict)

    return samples


def write_jsonl(data, filename):
    print(f"Writing results to {filename=}")
    with open(filename, 'w') as outfile:
        for entry in data:
            json.dump(entry, outfile)
            outfile.write('\n')


def read_problems(benchmark_name: str = "humanEval") -> Dict[str, Dict]:
    assert benchmark_name in ["humanEval", "mbpp", "sanitized-mbpp", "example_samples", "slice_sanitized-mbpp"], f"{benchmark_name=} is not a valid benchmark name"

    if benchmark_name == "humanEval":
        evalset_file = '../../../quadriton_main/quadriton/assets/code_eval/HumanEval.jsonl.gz'
    else:
        evalset_file = '../../../quadriton_main/quadriton/assets/code_eval/' + f"{benchmark_name}.jsonl"

    return {str(task["task_id"]): task for task in stream_jsonl(evalset_file)}


def stream_jsonl(filename: str) -> Iterable[Dict]:
    """
    Parses each jsonl line and yields it as a dictionary
    """
    if filename.endswith(".gz"):
        with open(filename, "rb") as gzfp:
            with gzip.open(gzfp, 'rt') as fp:
                for line in fp:
                    if any(not x.isspace() for x in line):
                        yield json.loads(line)
    else:
        with open(filename, "r") as fp:
            for line in fp:
                if any(not x.isspace() for x in line):
                    yield json.loads(line)


def create_prompt(data, benchmark):
    if benchmark == "humanEval":
        return data['prompt']+'\t\t'
    #if benchmark == "humanEval":
        #PROMPT = data['prompt']
        #return f"""You are an AI assistant that writes python code. User will give you a function definition to complete. Respond with the entire complete function definition, do not add any comments, be as concise in your code as possible.
    #User: Python function to complete without adding any comment:
    #{PROMPT}
    #\t\t
    #"""
        #return f"""[|Human|] Complete the following Python code: 
    #Notes: respond with the entire complete function definition
    #do not add any comments, be as concise in your code as possible
    #use only built-in libraries, assume no additional imports other than those provided (if any)

    #code:
    #```python
    #{PROMPT}
    #```

    #[|AI|]
    #```python
    #"""
    #if benchmark == "humanEval":
        #PROMPT = data['prompt']
        #return f"""[|Human|] Complete the following Python code: 
#Notes: respond with the entire complete function definition
#do not add any comments, be as concise in your code as possible
#use only built-in libraries, assume no additional imports other than those provided (if any)

#code:
#{PROMPT}

#[|AI|]
#```python
#"""
        #return f"""[|Human|] Please complete the following code:
#{PROMPT}

#[|AI|]
#```python
#"""
    elif benchmark in ["mbpp", "sanitized-mbpp", "slice_sanitized-mbpp"]:
        tests = '\n'.join(data['test_list'])
        return f"You are an expert Python programmer, and here is your task: {data['prompt']} Your code should pass these tests:\n\n{tests}\ndef "





def batch_generate(model, params, prompts, batch_size):
    all_results = []
    for i in list(range(0, len(prompts), batch_size)):
        requests = []
        for prompt in prompts[i: i + batch_size]:
            requests.append(GenerateRequest(inputs=prompt, parameters=params))
        start = time.time()
        response = model.create_from_objects(requests)
        stop = time.time()
        #print(stop-start)
        all_results += response
    return all_results


def remove_stop_words(input_string, stop_regex):
    result_string = stop_regex.sub('', input_string)
    result_string = result_string.rstrip('\n')
    return result_string


def generate_until(model,
                   prompts,
                   max_iterations,
                   params,
                   batch_size,
                   clean_regex,
                   stop_regex=r"\n(?!\s)"):
    current_iterations = 0
    prompt_candidates = list(enumerate(prompts))
    new_prompt_candidates, completed_solutions = [], []
    while (current_iterations < max_iterations) and (len(prompt_candidates) > 0):
        candidate_texts = [pc[1] for pc in prompt_candidates]
        candidate_idx = [pc[0] for pc in prompt_candidates]
        outputs = batch_generate(model, params, candidate_texts, batch_size)
        generated_texts = [remove_stop_words(gt, clean_regex).rstrip('\n') for gt in outputs]
        for idx in range(len(generated_texts)):
            trimmed_text = trim_stop_regex(generated_texts[idx], stop_regex)


            text_with_idx = (
                candidate_idx[idx],
                candidate_texts[idx] + trimmed_text[0]
            )
            if len(trimmed_text) != len(generated_texts[idx]) or (all(t == " " for t in trimmed_text)):
                completed_solutions.append(text_with_idx)
            else:
                new_prompt_candidates.append(text_with_idx)

        prompt_candidates = new_prompt_candidates
        new_prompt_candidates = []
        current_iterations += 1

    #for k in range(len(completed_solutions)):
        #print(completed_solutions[k])

    all_solutions = completed_solutions + prompt_candidates
    all_solutions.sort(key=lambda x: x[0])
    return all_solutions


def generate_solutions(model_name,
                       model,
                       output_folder,
                       mode,
                       temperature=0.2,
                       benchmark: str = "humanEval",
                       batch_size=16,
                       n_solutions_per_sample: int = 5,
                       max_token_per_iteration=100
                       ):
    
    print(f"{temperature=}")

    if benchmark == "humanEval":


        #stop_words = ["\nclass", "\ndef", "\n#", "\n@", "\nprint", "\nif", "\n```"]
        stop_words = ["\nclass", "\ndef",  "\nif", "\n```"]
        stop_regex = re.compile(r"(?i)(" + "|".join(re.escape(word) for word in stop_words) + ")")
        
        
    else:

        #stop_words= ["\nclass", "\nassert", '\n"""', "\nprint", "\nif", "\n<|/", "\n```"]
        stop_words= ["\nclass", "\nassert",  "\nif",  "\n```"]
        stop_regex = re.compile(r"(?i)(" + "|".join(re.escape(word) for word in stop_words) + ")")
        
        
    params = GenerateParameters(
                            max_new_tokens=args.max_token_per_iteration,
                            stop =stop_words,
                            temperature=temperature, 
                            top_p=0.95
                            )

    
    problems = read_problems(benchmark)

    sorted_items = sorted(problems.items(), key=lambda x: len(x[1]['prompt']), reverse=True)
    # Create a new ordered dictionary with sorted items
    problems = dict(sorted_items)

    
    n_samples = len(problems.values())
    #n_samples = 5

    clean_prompts = [problem['prompt'] for problem in problems.values()][:n_samples]
    repeated_clean_prompts = list(itertools.chain.from_iterable(itertools.repeat(x, 1) for x in clean_prompts))
    prompts = [create_prompt(problem, benchmark) for problem in problems.values()][:n_samples]
    repeated_prompts = list(itertools.chain.from_iterable(itertools.repeat(x, 1) for x in prompts))
    task_ids = [problem['task_id'] for problem in problems.values()][:n_samples]
    repeated_task_id = list(itertools.chain.from_iterable(itertools.repeat(x, 1) for x in task_ids))
    pbar = tqdm(total=n_solutions_per_sample)

    for i in range(n_solutions_per_sample):
        all_results = generate_until(model=model,
                                     params=params,
                                     max_iterations=6,
                                     batch_size=batch_size,
                                     prompts=repeated_prompts, 
                                     clean_regex=stop_regex)
        if benchmark == "humanEval":
            #completions = [repeated_clean_prompts[i][1]+all_results[i][1].split(repeated_prompts[i])[1] for i in range(len(repeated_prompts))]
            completions = [all_results[i][1] for i in range(len(repeated_prompts))]
            #completions = [all_results[i][1].split(repeated_prompts[i])[1] for i in range(len(repeated_prompts))]
        else:
            completions = ["def " + all_results[i][1].split(repeated_prompts[i])[1].rstrip('\n') for i in range(len(repeated_prompts))]

        df = pandas.DataFrame({'task_id': repeated_task_id, 'completion': completions})
        df.to_json(f"{output_folder}/solutions_{i}_{model_name.split('/')[0]}_{time.strftime('%H_%M_%S')}.jsonl",
                   lines=True, orient='records')

        pbar.update(1)



def parse_s3_path(s3_path):
    return s3_path.split("/")[2], "/".join(s3_path.split("/")[3:])


def get_tokenizer_name(checkpoint_name: str, tempdirpath):
    local_config_path = f"{tempdirpath}/config.json"
    cp_bucket, cp_key = parse_s3_path(checkpoint_name)
    s3_client.download_file(cp_bucket, f"{cp_key}/config.json", local_config_path)
    with open(local_config_path) as f:
        config = json.load(f)

    if "evaluation" in config.keys():
        return tokenizer_path[config["evaluation"]['tokenizer']]
    else:
        return tokenizer_path["gpt2"]


def deploy_endpoints(checkpoint_name, tokenizer):
    endpoint_names = []
    dev = False
    base_image_name = "quadriton-sm-inf-dev" if dev else "quadriton-sm-inf"
    extra_env_vars = {}
    if dev:
        dev_run_name = f"{uuid.uuid4()}-{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}"
        bucket, uploaded_path = upload_source_code_to_s3(dev_run_name, 'us-east-1')
        extra_env_vars['SOURCE_CODE_BUCKET'] = bucket
        extra_env_vars['SOURCE_CODE_PATH'] = uploaded_path
    #for i in range(4):
    #    endpoint_name = deploy(checkpoint_name, f"code-gen-endpoint-{i}", 'us-east-1', tokenizer)
    #    endpoint_names.append(endpoint_name)
    endpoint_name = deploy(checkpoint_name, "code-gen-endpoint", 'us-east-1', tokenizer, base_image_name=base_image_name)
    endpoint_names.append(endpoint_name)

    return endpoint_names


def launch_eval(checkpoint_s3_key, endpoint_names):

    for bm in ['humanEval']:
    #for bm in ['humanEval', 'sanitized-mbpp']:
    #for bm in ['sanitized-mbpp']:
    #for bm in ["example_samples"]:
    #for bm in ["slice_sanitized-mbpp"]:

        print(f"Launching eval of {checkpoint_s3_key} - {bm}")
        tempdirpath = tempfile.mkdtemp()
        print(f"temporary folder {tempdirpath}")
        launch = functools.partial(generate_solutions,
                                   temperature=float(args.temperature),
                                   benchmark=bm,
                                   mode=args.mode,
                                   batch_size=args.batch_size,
                                   output_folder=tempdirpath,
                                   n_solutions_per_sample=int(args.n_solutions_per_sample / len(endpoint_names)))
        processes = []
        model_name = '_'.join(checkpoint_s3_key.split('/')[-2:])
        for i in range(len(endpoint_names)):
  
            model = TGI(endpoint_name=endpoint_names[i], region_name='us-west-2')
            
            print("remote model has been called")
            """p = Process(target=launch, kwargs={"model": remote_model,
                                               "model_name": f"{model_name}_{i}"})"""
            p = Process(target=launch, kwargs={"model": model,
                                               "model_name": f"{model_name}_{i}"})
                                                     
            p.start()
            print(f"started process {p}")
            processes.append(p)

        print("exited the for loop for process generation")

        for process in processes:
            print(process)
            process.join()

        samples = read_generation_code(solution_folder=tempdirpath,
                                       benchmark_name=bm)
        ts = '-' + time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime()) 
        aggregate_results_filename = f"{model_name}_{bm}_{args.mode}_{args.temperature}_{args.n_solutions_per_sample}_{args.max_token_per_iteration}_{ts}.jsonl"
        write_jsonl(samples, f"{tempdirpath}/{aggregate_results_filename}")
        s3_client.upload_file(f"{tempdirpath}/{aggregate_results_filename}", "tii-llm-code-reboot", f"generations/completed/{aggregate_results_filename}")
        s3_client.upload_file(f"{tempdirpath}/{aggregate_results_filename}", "tii-llm-code-reboot", f"evaluations/{bm}/todo/{aggregate_results_filename}")
        print("Generation completed.")


def get_checkpoint_key(config_key, tempdirpath):
    local_config_path = f"{tempdirpath}/{os.path.basename(config_key)}"
    s3_client.download_file(bucket, config_key, local_config_path)
    with open(local_config_path) as f:
        config = json.load(f)
    return config["path"]


def check_s3():

    response = s3_client.list_objects_v2(Bucket=bucket, Prefix=prefix + "/todo/")
    if 'Contents' in response.keys() and len(response['Contents']) > 1:
        tempdirpath = tempfile.mkdtemp()
        config_key = response['Contents'][1]['Key']
        checkpoint_s3_key = get_checkpoint_key(config_key, tempdirpath)
        s3_client.delete_object(Bucket=bucket, Key=config_key)
        print(f"Found checkpoint {checkpoint_s3_key}, starting generation process.")
        tokenizer = get_tokenizer_name(checkpoint_s3_key, tempdirpath)
        #endpoint_names = deploy_endpoints(checkpoint_s3_key, tokenizer)
        endpoint_names = []
        for i in range(1):
            endpoint_names.append(deploy(checkpoint_s3_key, f"code-gen-endpoint-{i}"))
        launch_eval(checkpoint_s3_key, endpoint_names)
        check_s3()
    return

def alternative_s3(checkpoint_s3_key, endpoint_names):
    """response = s3_client.list_objects_v2(Bucket=bucket, Prefix=prefix + "/todo/")
    if 'Contents' in response.keys() and len(response['Contents']) > 1:
        tempdirpath = tempfile.mkdtemp()
        config_key = response['Contents'][1]['Key']
        checkpoint_s3_key = get_checkpoint_key(config_key, tempdirpath)
        s3_client.delete_object(Bucket=bucket, Key=config_key)
        print(f"Found checkpoint {checkpoint_s3_key}, starting generation process.")
        tokenizer = get_tokenizer_name(checkpoint_s3_key, tempdirpath)
        launch_eval(checkpoint_s3_key, endpoint_names)"""
    launch_eval(checkpoint_s3_key, endpoint_names)
    return

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Human Eval & MBPP generation parameters from s3')
    parser.add_argument('--temperature', "-t", type=str, required=False, default="0.2")
    parser.add_argument('--n_solutions_per_sample', "-n", type=int, required=False, default=100)
    parser.add_argument('--batch_size', "-bs", type=int, required=False, default=32)
    parser.add_argument('--mode', "-m", type=str, required=False, default="nucleus")
    parser.add_argument('--max_token_per_iteration', "-tokens", type=int, required=False, default=50)

    args = parser.parse_args()

    #assert args.n_solutions_per_sample % 2 == 0, "n_solutions must be divisible by 2"
    n_minutes_between_check = 100

    while True:
        now = datetime.now()
        print("\n######\n ", now.time())
        #check_s3()
        
        checkpoint_s3_key = "s3://sagemaker-us-west-2-968592933103/Quentin.Malartic/B10/B10_stage1-433282c3/B10_stage1/checkpoint_2500.3GT/"
        
        endpoint_names = [
                            "10B-2500GT-code-2024-01-23-07-08-15"
                        ]
        alternative_s3(checkpoint_s3_key, endpoint_names)
        sleep(n_minutes_between_check * 60)
