
from argparse import ArgumentParser
import argparse
from storage_utils import make_storage_path
import os
import time
import sagemaker
import boto3
import json
from sagemaker_executor import SagemakerExecutor

try:
	role = sagemaker.get_execution_role()
except ValueError:
	iam = boto3.client('iam')
	# role = iam.get_role(RoleName='sagemaker_execution_role')['Role']['Arn']
	# role = "arn:aws:iam::296133277686:SMRole"
	role = iam.get_role(RoleName="SMRole")["Role"]["Arn"]
 
def hf_convert_checkpoint(cp_path: str, tokenizer: str = 'noor_v3_mix', instance_type: str = 'ml.p4d.24xlarge' ,volume: int = 1000, container_image_uri: str = 'hf-conversion:latest'):
    from omegaconf import OmegaConf
    conf = eval("{'refiner': {'_target_': 'mdr.MacrodataRefiner', 'processors': [{'_target_': 'mdr.processors.readers.JsonlReader', 'data': 's3://sagemaker-us-east-1-296133277686/hamza.alobeidli/inference-dev/tgi-mdr-tools/'}, {'_target_': 'mdr.processors.SMLauncher', 'path': '', 'tokenizer': ' ', 'local_path': '/tmp/model/'}]}, 'executor': {'_target_': 'mdr.executors.SagemakerExecutor', 'job_name': 'hf_checkpoint_converter', 'logging_dir': '/tmp/logs/', 'container_image_uri': 'hf-conversion:latest', 'instance_type': 'ml.c5.18xlarge', 'instance_count': 1, 'tasks_per_machine': 1, 'workers_per_machine': 1, 'max_run': 36000, 'volume_size': 1000}}")
    config = OmegaConf.create(conf)
    account_id = boto3.client('sts').get_caller_identity()['Account']
    config.refiner.processors[0].data = f"s3://sagemaker-us-east-1-{account_id}/hamza.alobeidli/inference-dev/tgi-mdr-tools/"
    config.refiner.processors[1].path = cp_path
    config.refiner.processors[1].tokenizer = os.path.join('assets/tokenizer', tokenizer)
    config.refiner.processors[1].local_path = "/tmp/model/"
    config.executor.instance_type = instance_type
    config.executor.volume_size = volume
    config.executor.container_image_uri = container_image_uri
    executor = SagemakerExecutor(
        instance_count=1,
        total_tasks=-1,
        instance_type=instance_type,
        volume_size=volume,
        container_image_uri=container_image_uri,
        tasks_per_machine=1,
        job_name='hf_checkpoint_converter',
        logging_dir='/tmp/logs/',
        stage_i=0,
        max_run=72000,
        previous_stage=None,
        timestamp=config.executor.get('timestamp', time.strftime('%Y-%m-%d-%H-%M-%S', time.gmtime()))
    )
    executor.execute(config)

    
def tgi_sm_launcher(args):
    """First checkpoint if model is converted already to HF format. If not, the conversion starts"""
    cp_path = make_storage_path(args.path)
    is_hf_model_exist = False
    for file in cp_path.list_files():
        if file.path.endswith("model.tar.gz"):
            is_hf_model_exist = True
    
    if not is_hf_model_exist: #start conversion
        if args.tokenizer.endswith("tokenizer"):
            args.tokenizer = args.tokenizer.split("/")[0]
        hf_convert_checkpoint(cp_path=args.path, tokenizer=args.tokenizer)
    
    """Now Endpoint deployment"""
    if args.timestamp:
        endpoint_name = f"{args.name}-{time.strftime('%Y-%m-%d-%H-%M-%S', time.gmtime())}"
    else:
        endpoint_name = f"{args.name}"
        
    # # Hub Model configuration. https://huggingface.co/models
    # 40B
    """ hub = {
        'HF_MODEL_ID':'/opt/ml/model',
        'SM_NUM_GPUS': json.dumps(8),
        'SAGEMAKER_MODEL_SERVER_TIMEOUT': '900000',
        'MAX_INPUT_LENGTH': '2047',
        'MAX_TOTAL_TOKENS': '2048',
        'MAX_BATCH_PREFILL_TOKENS': '8192',
        'SHARDED': 'true',
        'NUM_SHARD': '8'
    } """
    # 7B 
    hub = {
        'HF_MODEL_ID':'/opt/ml/model',
        'SM_NUM_GPUS': json.dumps(1),
        'SAGEMAKER_MODEL_SERVER_TIMEOUT': '900000',
        'MAX_INPUT_LENGTH': '2048',
        'MAX_TOTAL_TOKENS': '2049',
        'MAX_BATCH_PREFILL_TOKENS': '8192',
    }
    from sagemaker.huggingface.model import HuggingFaceModel
    os.environ['AWS_DEFAULT_REGION'] = args.region
    account_id = boto3.client('sts').get_caller_identity()['Account']
    # create Hugging Face Model Class
    huggingface_model = HuggingFaceModel(
        image_uri=f"{account_id}.dkr.ecr.{args.region}.amazonaws.com/tgi-tii-sm:latest",
        model_data=os.path.join(args.path, "model.tar.gz"),
        env=hub,
        role=role,
    )
    
    config = make_storage_path(args.path).join("config.json").load_config()

    if config.model.d_model < 2048:
        instance_type = "ml.g5.xlarge"
    elif config.model.d_model <= 4096:
        instance_type = "ml.p4d.24xlarge"#"ml.g5.8xlarge"
    elif config.model.d_model < 5500:
        instance_type = "ml.p4d.24xlarge"#"ml.g5.12xlarge"
    else:
        print("CHONKY BOI DETECTED -- UNLEASHING THE P4D")
        #instance_type = "ml.p4d.24xlarge"  # TODO: figure out how to run it on 8x A10
        instance_type = "ml.p4d.24xlarge"  
    
    print(f"Waiting for {endpoint_name} endpoint to be in service...")
    # deploy model to SageMaker Inference
    huggingface_model.deploy(
        endpoint_name=f"{endpoint_name}",
        model_data_download_timeout=3600,
        initial_instance_count=1,
        instance_type=instance_type, #"ml.g5.8xlarge", #"ml.g5.2xlarge", #"ml.p4d.24xlarge",
        container_startup_health_check_timeout=300,
        trust_remote_code=True,
    )
    print("=" * 20)
    print("Endpoint name: " + endpoint_name)
    print("=" * 20)
    
    return endpoint_name


def quadriton_sm_launcher(args):
    import uuid
    from quadriton.tools.sm_deploy_checkpoint import main, upload_source_code_to_s3
    from quadriton.client.remote_model import RemoteModel
    from quadriton.client.communicator import SagemakerCommunicator
    
    extra_env_vars = {}
    dev_run_name = f"{uuid.uuid4()}-{time.strftime('%Y-%m-%d-%H-%M-%S', time.gmtime())}"
    bucket, uploaded_path = upload_source_code_to_s3(dev_run_name, 'us-east-1')
    extra_env_vars['SOURCE_CODE_BUCKET'] = bucket
    extra_env_vars['SOURCE_CODE_PATH'] = uploaded_path
    if args.timestamp:
        endpoint_name = f"{args.name}-{time.strftime('%Y-%m-%d-%H-%M-%S', time.gmtime())}"
    else:
        endpoint_name = f"{args.name}"
    main(path=args.path, name=endpoint_name, region='us-east-1', tokenizer=args.tokenizer, timestamp=False,
    base_image_name="quadriton-sm-inf-dev", **extra_env_vars)
    print("\033[1mWaiting for the endpoint to be ready...\033[0m")
    trial = 0
    while trial < 25:
        try:
            model: RemoteModel = RemoteModel(comm=SagemakerCommunicator(endpoint_name+'-ep'))
            break
        except Exception as e:
            print("Endpoint not ready yet...")
            trial += 1
            time.sleep(440)
            if trial >=25:
                raise e
    return endpoint_name+'-ep'


def hf_sm_launcher(args):
    from sagemaker.pytorch import PyTorchModel
    iam = boto3.client("iam")
    role = iam.get_role(RoleName="SMRole")["Role"]["Arn"]
    env = {
    "SAGEMAKER_TS_BATCH_SIZE": "1",
    "SAGEMAKER_TS_MAX_BATCH_DELAY": "100",
    "SAGEMAKER_TS_MIN_WORKERS": "1",
    "SAGEMAKER_TS_MAX_WORKERS": "1",
    "SAGEMAKER_MODEL_SERVER_TIMEOUT": "900000",
    }
    if not args.path.endswith("model.tar.gz"):
        args.path = os.path.join(args.path, "model.tar.gz")
    pt_model = PyTorchModel(
    model_data=args.path,
    role=role,
    framework_version="2.0",
    py_version="py310",
    env=env,
    )
    if args.timestamp:
        endpoint_name = f"{args.name}-{time.strftime('%Y-%m-%d-%H-%M-%S', time.gmtime())}"
    else:
        endpoint_name = f"{args.name}"
    
    print(f"Waiting for {endpoint_name} endpoint to be in service...")
    pt_model.deploy(
    # endpoint_name=f"jais-13b-16bf-{time.strftime('%Y-%m-%d-%H-%M-%S')}",
    endpoint_name=endpoint_name,
    initial_instance_count=1,
    instance_type="ml.p4d.24xlarge",
    model_server_workers=1,
    serializer=sagemaker.serializers.JSONSerializer(),
    deserializer=sagemaker.deserializers.JSONDeserializer(),
    )
    print("=" * 20)
    print("Endpoint name: " + endpoint_name)
    print("=" * 20)
    return endpoint_name
    
    

if __name__ == "__main__":
    parser = ArgumentParser(description="TGI Sagemaker Endpoint Launcher")
    parser.add_argument('-p', '--path', help='s3 path with the model checkpoint to deploy')
    parser.add_argument('-n', '--name', default='tgi-model', help='endpoint name to use')
    parser.add_argument('-r', '--region', default='us-east-1', help='aws region')
    parser.add_argument('--timestamp', action=argparse.BooleanOptionalAction, default=True)
    parser.add_argument('-t', '--tokenizer', default='dd_alpha', help='tokenizer to use (example: `gpt2`)')
    parser.add_argument('--model_configure', default='assets/models/falcon_noor/', help='path to model congiration')    
    args = parser.parse_args()
    if 'us-east-1' in args.path:
        args.region = 'us-east-1'
    elif 'us-west-2' in args.path:
        args.region = 'us-west-2'
    
    tgi_sm_launcher(args)    
    