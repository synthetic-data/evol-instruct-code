import random
from TextGenerationInference import TGI, GenerateRequest, GenerateParameters
import json 
import re
import time 
from tqdm import tqdm as tqdm_bar

tgi_client = TGI(endpoint_name="180B-Evoli-v9-2024-03-13-12-23-24", region_name="us-east-1")

#seed_tasks_path = "./merged_good_codealpaca.json"
seed_tasks_path = "./converted_alpaca_20k.json"

def load_instructions(file_path: str):
    """Load JSON file in Evol Format"""
    with open(file_path, "r") as json_file:
        return json.load(json_file)

def evolve_instructions(instructions, tgi):
    methods = [
        'Add new constraints and requirements to the original problem, adding approximately 10 additional words.',
        'Replace a commonly used requirement in the programming task with a less common and more specific one.',
        'If the original problem can be solved with only a few logical steps, please add more reasoning steps.',
        'Provide a piece of erroneous code as a reference to increase misdirection.',
        'Propose higher time or space complexity requirements, but please refrain from doing so frequently.'
    ]

    tasks_evolution = []

    # Generate prompts for batch processing
    prompts = []
    for task in instructions:
        chosen_method = random.choice(methods)
        prompt = f"Please increase the difficulty of the given programming test question a bit without using the following method: '{chosen_method}'.\n\n#Given Test#\n{task['instruction']}\n\n#Rewritten Test#\n"
        prompts.append(prompt)

    # Define parameters for text generation
    params = GenerateParameters(max_new_tokens=512, temperature=0.7, stop=["#Rewritten Test#", "\n\n", "def ", "\"\"\""])

    # Batch generate responses
    batch_size = 64
    batch_responses = batch_generate(tgi, params, prompts, batch_size)

    # Parse responses and create tasks_evolution
    for i, task in enumerate(instructions):
        chosen_method = random.choice(methods)
        response = batch_responses[i]
        
        task_evolution = {
            "original_instruction": task['instruction'],
            "original_output" : task['output'],
            "chosen_method": chosen_method,
            "rewritten_instruction": response
        }
        tasks_evolution.append(task_evolution)

    return tasks_evolution

def generate_responses(tasks_evolution, tgi):
    prompts = []
    for task in tasks_evolution:
        # Use rewritten_instruction to generate a response
        prompt = task["rewritten_instruction"] + '\n#Code#'
        prompts.append(prompt)
        
    # Define parameters for text generation
    params = GenerateParameters(
        max_new_tokens=512, 
        temperature=0.2, 
        stop=["#Code#", "#Result#", "#Output#", "#Explanation#"]
    )

    # Batch generate responses
    batch_size = 64
    batch_responses = batch_generate(tgi, params, prompts, batch_size)

    # Update tasks_evolution with rewritten outputs
    for i, task in enumerate(tasks_evolution):
        rewritten_response = batch_responses[i]
        task["rewritten_output"] = rewritten_response

    return tasks_evolution

def batch_generate(model, params, prompts, batch_size):
    all_results = []
    for i in tqdm_bar(range(0, len(prompts), batch_size), desc="Generating"):
        batch_prompts = prompts[i: i + batch_size]
        requests = [GenerateRequest(inputs=prompt, parameters=params) for prompt in batch_prompts]
        batch_responses = model.create_from_objects(requests)
        all_results.extend(batch_responses)
    return all_results

def count_instructions(dataset):
    """
    Count the number of instructions in the dataset.
    """
    return len(dataset)

if __name__ == "__main__":
    start_time = time.time()
    prev_tasks = load_instructions(seed_tasks_path)
    
    evolutions = 3
    all_tasks = []  # List to store tasks for all evolutions
    for evolution in range(1, evolutions + 1):
        print(f'Evolution {evolution}:')
        print("Generating New Instructions and their answers")
        final_tasks = []
        final_tasks.extend(generate_responses(evolve_instructions(prev_tasks, tgi_client), tgi_client))
        all_tasks.extend(final_tasks)  # Add tasks for the current evolution to the list
        print('Done !')
    end_time = time.time()
    execution_time = end_time - start_time
    print(f"Total execution time: {execution_time} seconds")

    # Write all tasks to a single JSON file
    with open('all_evolution_tasks_from_alpaca_evols_v9_180B.json', 'w') as json_file:
        json.dump(all_tasks, json_file, indent=4)
