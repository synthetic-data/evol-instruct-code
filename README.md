# 🪵 Welcome to the Evol-Instruct-Code repo


## Falcon-40B

In this folder, you will find all the codes that have been used to generate code data with Falcon-40B fine-tuned.

#### How to use
1. Deploy an endpoint with ```sm_endpoint_launcher.py``` or any other scripts that allows you to do so
Example :
```sh 
python3 sm_endpoint_launcher.py -p s3://sagemaker-us-west-2-968592933103/alice.pagnoux/synthetic-data-finetuning/10B-av1cli-codeT-128-1.00e-06-9702277b/10B-av1cli-codeT-128-1.00e-06/checkpoint_2.1MT/ -r us-east-1 -n <name_of_your_endpoint>
```
2. In ```generate.py```, change the name of the endpoint and region
3. Name your new dataset at the end of the script



## CodeLlamaInstruct-70B

In this folder, you will find all the codes that have been used to generate code data with CodeLlamaInstruct-70B.

#### How to use
1. Deploy an endpoint from HuggingFace with ```hf_deploy_endpoint.py``` or any other scripts that allows you to do so
2. In ```generate.py```, change the name of the endpoint and region
3. Name your new dataset at the end of the script

⚠️ Some filters are already applied in ```generate.py```.

## Filters_and_Mergers

In this folder, you will find all the scripts used to filter out the datasets generated, as well as a converter (from json to jsonl) and a merger that allows you to merge your dataset with Codealpaca.



#### Personal tips on how to push local code to Gitlab

1. Check if SSH keys are generated :
```sh
ls -la ~/.ssh
```
Look for 'id_rsa' and 'id_rsa_pub'. If they don't exist, create them with the following :
```sh
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
2. Copy your public SSH key :
```sh
cat ~/.ssh/id_rsa.pub
```
3. In your Gitlab account, navigate to Settings > SSH Keys. Paste your key in the field and click on 'Add key'.
4. In your terminal, verify that you are connected to Gitlab :
```sh
ssh -T git@gitlab.com
```

